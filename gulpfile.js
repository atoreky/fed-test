const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const terser = require('gulp-terser');
const clean = require('gulp-clean');
const browsersync = require('browser-sync').create();

// Clean Task
function cleanTask(){
  return src(['dist/styles', 'dist/js', 'dist/fonts', 'dist/img'], { read: false, allowEmpty: true })
    .pipe(clean());
}

// Sass Task
function scssTask(){
  return src('app/scss/style.scss', { sourcemaps: true })
    .pipe(sass())
    .pipe(postcss([cssnano()]))
    .pipe(dest('dist/styles', { sourcemaps: '.' }));
}

// JavaScript Task
function jsTask(){
  return src('app/js/script.js', { sourcemaps: true })
    .pipe(terser())
    .pipe(dest('dist/js', { sourcemaps: '.' }));
}

// Copy fonts Task
function copyFonts(){
  return src('app/fonts/*')
    .pipe(dest('dist/fonts'));
}

// Copy images Task
function copyImages(){
  return src('app/img/*')
    .pipe(dest('dist/img'));
}

// Copy dependencies
function copyDependencies(){
  return src([
    'node_modules/jquery/dist/jquery.min.js', 
    'node_modules/jquery-validation/dist/jquery.validate.min.js'
    ])
    .pipe(dest('dist/js'));
}

// Browsersync Tasks
function browsersyncServe(cb){
  browsersync.init({
    server: {
      baseDir: '.'
    }
  });
  cb();
}

function browsersyncReload(cb){
  browsersync.reload();
  cb();
}

// Watch Task
function watchTask(){
  watch('*.html', browsersyncReload);
  watch(['app/scss/**/*.scss', 'app/js/**/*.js'], series(scssTask, jsTask, browsersyncReload));
}

// Default Gulp task
exports.default = series(
  cleanTask,
  scssTask,
  jsTask,
  copyFonts,
  copyImages,
  copyDependencies,
  browsersyncServe,
  watchTask
);
$(function() {

    // Mobile navigation
    $(".navbar-burger").on("click", function() {
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active");
        $("html").toggleClass("mobile-nav-active");
    });

    // Footer form validation
    $("form[name='subscribe']").validate({
        rules: {
            firstname: "required",
            lastname: "required",
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            firstname: "Please enter your first name",
            lastname: "Please enter your last name",
            email: {
                required: "Please enter your email address",
                email: "Please enter a valid email address"
            },
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

    // Footer scrolltop
    $(".go-top").on("click", function() {
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
    });

    // Blog posts
    $.getJSON("data.json", function(data) {
        var postItems = '';

        $.each(data, function (key, value) {
            postItems += '<div class="column post is-4">';
            postItems += '<div class="post-item">';
            postItems += '<a href="#"><img class="post-item-img" src="' + value.image + '"></a>';
            postItems += '<div class="post-item-bottom"><h3 class="post-item-title"><a href="#">' + value.title + '</a></h3>';
            postItems += '<a class="post-item-link" href="#">Read more <span class="icon icon-arrow-right"></span></a>';
            postItems += '</div></div></div>';
        });

        $('.block-posts').append(postItems);
    });
});
